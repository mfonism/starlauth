from asyncpg.exceptions import UniqueViolationError
from pydantic import ValidationError
from starlette.exceptions import HTTPException
from starlette.requests import Request
from starlette.responses import JSONResponse

from . import utils
from .pydantic_models import UserModel
from .resources import db
from .tables import users


@db.transaction()
async def create_user(request: Request):
    _data = await request.json()
    try:
        validated_data = UserModel(**_data).dict()
    except ValidationError as exc:
        errors = (
            {
                "type": error["type"].replace("_", " "),
                "field": error["loc"][0],
                "message": error["msg"],
            }
            for error in exc.errors()
        )
        raise HTTPException(status_code=400, detail={"errors": list(errors)}) from exc

    timestamp = utils.get_utcnow()
    user_insertion_stmt = users.insert().values(  # noqa: E1120
        email=validated_data["email"],
        username=validated_data["username"],
        casefolded_username=validated_data["username"].casefold(),
        passphrase=utils.get_passphrase_hash(validated_data["passphrase"]),
        display_name=validated_data["display_name"],
        created_at=timestamp,
    )

    try:
        await db.execute(user_insertion_stmt)
    except UniqueViolationError as exc:
        if exc.detail.startswith("Key (email)"):  # noqa: E1101
            raise HTTPException(
                status_code=409,
                detail={
                    "error": {
                        "type": "uniqueness constraint violation",
                        "field": "email",
                        "message": "a user with this email address already exists",
                    }
                },
            ) from exc
        if exc.detail.startswith(  # noqa: E1101
            "Key (username)"
        ) or exc.detail.startswith(  # noqa: E1101
            "Key (casefolded_username)"
        ):
            raise HTTPException(
                status_code=409,
                detail={
                    "error": {
                        "type": "uniqueness constraint violation",
                        "field": "username",
                        "message": "a user with this username already exists",
                    }
                },
            ) from exc
        raise

    return JSONResponse(
        status_code=201,
        content={
            "data": {
                "email": validated_data["email"],
                "username": validated_data["username"],
                "display_name": validated_data["display_name"],
            }
        },
    )
