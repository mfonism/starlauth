import base64
import hashlib
from datetime import datetime, timezone
from typing import Optional

import bcrypt
from itsdangerous.exc import BadData, SignatureExpired

from .exceptions import ExpiredToken, InvalidToken
from .tokens import email_confirmation_token_generator


def get_utcnow() -> datetime:
    return datetime.now(timezone.utc)


def get_safe_sha256_digest(plaintext: str) -> bytes:
    # bytes = plaintext.encode()
    # digest = hashlib.sha256(bytes).digest()
    # safe_digest = base64.b64encode(digest)
    return base64.b64encode(hashlib.sha256(plaintext.encode()).digest())


def get_passphrase_hash(plaintext: str) -> str:
    return bcrypt.hashpw(get_safe_sha256_digest(plaintext), bcrypt.gensalt()).decode()


def check_passphrase(plaintext: str, hashed: str) -> bool:
    return bcrypt.checkpw(get_safe_sha256_digest(plaintext), hashed.encode())


def get_email_confirmation_token(email: str) -> str:
    return email_confirmation_token_generator.dumps(email)


def decode_email_confirmation_token(token: str, max_age: Optional[int] = None) -> str:
    try:
        return email_confirmation_token_generator.loads(token, max_age=max_age)
    except SignatureExpired as exc:
        raise ExpiredToken from exc
    except BadData as exc:
        raise InvalidToken from exc
